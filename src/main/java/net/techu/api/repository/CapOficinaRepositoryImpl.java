package net.techu.api.repository;

import net.techu.api.models.CapacidadOficina;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;


@Repository
public class CapOficinaRepositoryImpl implements CapOficinaRepository{

    private final MongoOperations mongoOperations;

    @Autowired
    public CapOficinaRepositoryImpl(MongoOperations mongoOperations){
        this.mongoOperations = mongoOperations;
    }

    @Override
    public CapacidadOficina buscaOficina(String id) {
        CapacidadOficina encontrado = this.mongoOperations.findOne(new Query(Criteria.where("idOficina").is(id)), CapacidadOficina.class);
        return encontrado;
    }

    @Override
    public void IncrementaAforoActualOficina(String id) {
        CapacidadOficina capacidadOficina = buscaOficina(id);
        if(capacidadOficina==null) {
            capacidadOficina= new CapacidadOficina();
            capacidadOficina.setIdOficina(id);
            capacidadOficina.setAforoTotal(10);
            capacidadOficina.setAforoActual(1);
            this.mongoOperations.save(capacidadOficina);
        }else
            {
                Query query = new Query(new Criteria("idOficina").is(id));
                Update update = new Update().set("aforoActual", capacidadOficina.getAforoActual()+1);
                this.mongoOperations.updateFirst(query, update, CapacidadOficina.class);
            }

    }


}
