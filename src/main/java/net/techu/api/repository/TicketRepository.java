package net.techu.api.repository;

import net.techu.api.models.Ticket;

public interface TicketRepository {
    public Ticket saveTicket(Ticket t);
    public Ticket findOne(long numeroTicket);

}
