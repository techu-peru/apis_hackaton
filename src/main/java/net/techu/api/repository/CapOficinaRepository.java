package net.techu.api.repository;

import net.techu.api.models.CapacidadOficina;

public interface CapOficinaRepository {
    public CapacidadOficina buscaOficina(String id);
    public void  IncrementaAforoActualOficina(String id);
}
