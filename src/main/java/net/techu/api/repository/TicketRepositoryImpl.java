package net.techu.api.repository;

import net.techu.api.models.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.sql.Timestamp;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.Date;


@Repository
public class TicketRepositoryImpl implements TicketRepository{

    private final MongoOperations mongoOperations;
    @Autowired
    public TicketRepositoryImpl(MongoOperations mongoOperations){
        this.mongoOperations = mongoOperations;
    }
    @Override
    public Ticket saveTicket(Ticket t) {

        long  i=this.mongoOperations.count(new Query(),Ticket.class);
        i++;
        t.setNumberTicket(i);

        Date now = new Date();

        t.setFechaHora(now);

        this.mongoOperations.save(t);

         return findOne(i);
    }

    @Override
    public Ticket findOne(long numberTicket) {
        Ticket encontrado = this.mongoOperations.findOne(new Query(Criteria.where("numberTicket").is(numberTicket)),Ticket.class);
        return encontrado;
    }

}
