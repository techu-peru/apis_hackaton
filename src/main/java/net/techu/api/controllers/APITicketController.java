package net.techu.api.controllers;

import net.techu.api.models.Capacidad;
import net.techu.api.models.CapacidadOficina;
import net.techu.api.models.Ticket;
import net.techu.api.services.CapOficinaService;
import net.techu.api.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST})
public class APITicketController {
    private final TicketService ticketService;
    private final CapOficinaService capacidadOficinaService;
    @Autowired
    public APITicketController(TicketService ticketService, CapOficinaService capacidadOficinaService){
        this.ticketService = ticketService;
        this.capacidadOficinaService = capacidadOficinaService;
    }

    @PostMapping(value="/ticket")
    public ResponseEntity<Ticket> saveTicket(@RequestBody Ticket ticket){
        Ticket ticketCreado = ticketService.saveTicket(ticket);
        if(ticketCreado!=null){
            if(ticketCreado.getNumberTicket()>0){
                capacidadOficinaService.IncrementaAforoActualOficina(ticket.getIdOffice());
                return new ResponseEntity<Ticket>(ticketCreado, HttpStatus.CREATED);
            }
            return new ResponseEntity<Ticket>(HttpStatus.FAILED_DEPENDENCY);
        }
        return new ResponseEntity<Ticket>( HttpStatus.FAILED_DEPENDENCY);
    }
}
