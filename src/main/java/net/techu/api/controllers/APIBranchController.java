package net.techu.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.techu.api.GestorRest;
import net.techu.api.models.*;
import net.techu.api.services.CapOficinaService;
import net.techu.api.services.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class APIBranchController {
    private final CapOficinaService capacidadOficinaService;
    @Autowired
    public APIBranchController(CapOficinaService capacidadOficinaService){
        this.capacidadOficinaService = capacidadOficinaService;
    }
    @GetMapping(value="/branches")
    public ResponseEntity<RespuestaListadoOficina> getBranches(@RequestParam(name="latitude", required=true) String latitude,
                                                  @RequestParam (name="longitude" ,required=true) String longitude,
                                                  @RequestParam (name="radius",required=false) String radius){

        UriComponents uri = UriComponentsBuilder.newInstance()
                .scheme("https").host("glomo.bbva.pe/SRVS_A02/pois/v0/branches")
                .queryParam("latitude",latitude).queryParam("longitude",longitude)
                .queryParam("radius",radius).build();

        System.out.println(uri.toString());
        //Ejemplo->https://glomo.bbva.pe/SRVS_A02/pois/v0/branches?latitude=-12.0969652&longitude=-77.0144793&radius=1000
        HttpHeaders headers = new HttpHeaders();
        String token= getTokenPublico();
        if(token != null)
        {
            headers.set("tsec",token);
            headers.setContentType(MediaType.APPLICATION_JSON);
            GestorRest gestor = new GestorRest();
            RestTemplate plantilla = gestor.crearClienteRest(new RestTemplateBuilder());
            HttpEntity request = new HttpEntity(headers);
            /*
            ResponseEntity<String> listado = plantilla.exchange(uri.toString(), HttpMethod.GET, request,
                    String.class);
            return listado;
            */

            ResponseEntity<RespuestaListadoOficina> listado = plantilla.exchange(uri.toString(), HttpMethod.GET,
                    request,RespuestaListadoOficina.class);
            List<Oficina>listadoOficina = listado.getBody().getData();
            if (listado.getStatusCode()==HttpStatus.OK){
                if(listadoOficina.size()==0){
                    return new ResponseEntity<RespuestaListadoOficina>(HttpStatus.NO_CONTENT);
                }



                for (int i=0;i < listadoOficina.size(); i++) {
                    String estado = "S";
                    Capacidad capacidad = new Capacidad(10,0,estado);
                    CapacidadOficina capacidadOficina = capacidadOficinaService.buscaOficina(listadoOficina.get(i).getId());

                    if(capacidadOficina!=null){

                        capacidad.setAforoActual(capacidadOficina.getAforoActual());
                        capacidad.setAforoTotal(capacidadOficina.getAforoTotal());

                        float Ocupacion = (float) capacidadOficina.getAforoActual() / (float) capacidadOficina.getAforoTotal();
                        if (Ocupacion <= 0.30) {
                            estado = "S";
                        } else {
                            if (Ocupacion > 0.70) {
                                estado = "L";
                            }else{
                                estado = "M";
                            }
                        }
                        capacidad.setStatus(estado);

                    }
                    listadoOficina.get(i).setCapacidad(capacidad);
                }

                return new ResponseEntity<RespuestaListadoOficina>(listado.getBody(),HttpStatus.OK);
            }
            //List<Oficina> data = response.getBody().getValue();*/
            }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    private String getTokenPublico(){

        AuthenticationRequest authenticationRequest = new AuthenticationRequest();
        authenticationRequest.setUserID("13000014");
        authenticationRequest.setConsumerID("13000014");
        authenticationRequest.setAuthenticationType("61");
        authenticationRequest.setAuthenticationData(new Object[0]);
        BackendUserRequest backendUserRequest = new BackendUserRequest();
        backendUserRequest.setAccessCode("");
        backendUserRequest.setDialogId("");
        backendUserRequest.setUserId("");
        LoginPublico loginPublico= new LoginPublico(authenticationRequest,backendUserRequest);

        //final String uri="https://glomo.bbva.pe/SRVS_A02/TechArchitecture/pe/grantingTicket/V02";
        UriComponents uri = UriComponentsBuilder.newInstance()
                .scheme("https").host("glomo.bbva.pe/SRVS_A02/TechArchitecture/pe/grantingTicket/V02")
                .build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        /*ObjectMapper objectMapper = new ObjectMapper();

        try {
            objectMapper.writeValue(new File("target/login_publico.json"), loginPublico);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        HttpEntity<LoginPublico> request = new HttpEntity<>(loginPublico, headers);
        GestorRest gestor = new GestorRest();
        RestTemplate plantilla = gestor.crearClienteRest(new RestTemplateBuilder());

        ResponseEntity<RespuestaLoginPublico> respuestaLoginPublico = plantilla.exchange(uri.toString(), HttpMethod.POST,
                request,RespuestaLoginPublico.class);
        System.out.println("Respuesta Login Publico->"+respuestaLoginPublico.getStatusCode());
        if(respuestaLoginPublico.getStatusCode() == HttpStatus.OK){
             String token = respuestaLoginPublico.getHeaders().getFirst("tsec");
             System.out.println("token->"+token);
            return token;
        }
        return null;
    }

}
