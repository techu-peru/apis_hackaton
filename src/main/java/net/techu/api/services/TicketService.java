package net.techu.api.services;

import net.techu.api.models.Ticket;

public interface TicketService {
    public Ticket saveTicket(Ticket t);
}
