package net.techu.api.services;

import net.techu.api.models.CapacidadOficina;

public interface CapOficinaService {
    public CapacidadOficina buscaOficina(String id);
    public void  IncrementaAforoActualOficina(String id);
}
