package net.techu.api.services;

import net.techu.api.models.CapacidadOficina;
import net.techu.api.repository.CapOficinaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("CapOficinaService")
@Transactional
public class CapOficinaServiceImpl implements CapOficinaService{
    private CapOficinaRepository capOficinaRepository;
    @Autowired
    public CapOficinaServiceImpl(CapOficinaRepository capOficinaRepository){
        this.capOficinaRepository = capOficinaRepository;
    }

    @Override
    public CapacidadOficina buscaOficina(String id) {
        return capOficinaRepository.buscaOficina(id);
    }

    @Override
    public void IncrementaAforoActualOficina(String id) {
        capOficinaRepository.IncrementaAforoActualOficina(id);
    }
}
