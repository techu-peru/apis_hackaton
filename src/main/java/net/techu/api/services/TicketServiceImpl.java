package net.techu.api.services;

import net.techu.api.models.Ticket;
import net.techu.api.repository.CapOficinaRepository;
import net.techu.api.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("TicketService")
@Transactional
public class TicketServiceImpl implements TicketService{
    private TicketRepository ticketRepository;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository){
        this.ticketRepository = ticketRepository;
    }


    @Override
    public Ticket saveTicket(Ticket t) {

        return ticketRepository.saveTicket(t);
    }
}
