package net.techu.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
@Document(collection = "CapacidadOficina")
@JsonPropertyOrder({"id", "aforoTotal", "aforoActual"})
public class CapacidadOficina implements Serializable {

    @NotNull
    @JsonProperty("id")
    private String idOficina;

    private Integer aforoTotal;
    private Integer aforoActual;

    public CapacidadOficina() {}

    public CapacidadOficina(String idOficina, Integer aforoTotal, Integer aforoActual)
    {
        this.setIdOficina(idOficina);
        this.setAforoTotal(aforoTotal);
        this.setAforoActual(aforoActual);
    }

    public String getIdOficina() {
        return idOficina;
    }

    public void setIdOficina(String idOficina) {
        this.idOficina = idOficina;
    }

    public Integer getAforoTotal() {
        return aforoTotal;
    }

    public void setAforoTotal(Integer aforoTotal) {
        this.aforoTotal = aforoTotal;
    }

    public Integer getAforoActual() {
        return aforoActual;
    }

    public void setAforoActual(Integer aforoActual) {
        this.aforoActual = aforoActual;
    }
}
