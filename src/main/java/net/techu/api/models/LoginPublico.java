package net.techu.api.models;

public class LoginPublico {

    private AuthenticationRequest authentication;
    private BackendUserRequest backendUserRequest;

    public LoginPublico(AuthenticationRequest authentication, BackendUserRequest backendUserRequest) {
        this.authentication = authentication;
        this.backendUserRequest = backendUserRequest;
    }

    public AuthenticationRequest getAuthentication() {
        return authentication;
    }

    public void setAuthentication(AuthenticationRequest authentication) {
        this.authentication = authentication;
    }

    public BackendUserRequest getBackendUserRequest() {
        return backendUserRequest;
    }

    public void setBackendUserRequest(BackendUserRequest backendUserRequest) {
        this.backendUserRequest = backendUserRequest;
    }
}
