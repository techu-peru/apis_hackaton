package net.techu.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GeographicGroups implements Serializable {
    @JsonProperty("name")
    private String name;
    @JsonProperty("geographicGroupType")
    private GeographicGroupType geographicGroupType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GeographicGroupType getGeographicGroupType() {
        return geographicGroupType;
    }

    public void setGeographicGroupType(GeographicGroupType geographicGroupType) {
        this.geographicGroupType = geographicGroupType;
    }
}
