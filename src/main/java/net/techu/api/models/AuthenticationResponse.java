package net.techu.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AuthenticationResponse implements Serializable {
    @JsonProperty("authenticationState")
    private String authenticationState;
    @JsonProperty("authenticationData")
    private Object authenticationData[];


    public String getAuthenticationState() {
        return authenticationState;
    }

    public void setAuthenticationState(String authenticationState) {
        this.authenticationState = authenticationState;
    }

    public Object[] getAuthenticationData() {
        return authenticationData;
    }

    public void setAuthenticationData(Object[] authenticationData) {
        this.authenticationData = authenticationData;
    }
}
