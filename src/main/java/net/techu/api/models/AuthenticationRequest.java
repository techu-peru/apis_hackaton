package net.techu.api.models;

import java.util.Arrays;

public class AuthenticationRequest {
    private String userID;
    private String consumerID;
    private String authenticationType;
    private Object authenticationData[];

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getConsumerID() {
        return consumerID;
    }

    public void setConsumerID(String consumerID) {
        this.consumerID = consumerID;
    }

    public String getAuthenticationType() {
        return authenticationType;
    }

    public void setAuthenticationType(String authenticationType) {
        this.authenticationType = authenticationType;
    }

    public Object[] getAuthenticationData() {
        return authenticationData;
    }

    public void setAuthenticationData(Object[] authenticationData) {
        this.authenticationData = authenticationData;
    }
}
