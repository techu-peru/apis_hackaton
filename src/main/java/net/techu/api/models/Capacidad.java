package net.techu.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
@JsonIgnoreProperties(ignoreUnknown = true)
public class Capacidad implements Serializable {
    @JsonProperty("totalCapacity")
    private int aforoTotal;
    @JsonProperty("currentCapacity")
    private int aforoActual;
    @JsonProperty("status")
    private String status;

    public Capacidad() {
    }

    public Capacidad(int aforoTotal, int aforoActual, String status) {
        this.aforoTotal = aforoTotal;
        this.aforoActual = aforoActual;
        this.status = status;
    }

    public int getAforoTotal() {
        return aforoTotal;
    }

    public void setAforoTotal(int aforoTotal) {
        this.aforoTotal = aforoTotal;
    }

    public int getAforoActual() {
        return aforoActual;
    }

    public void setAforoActual(int aforoActual) {
        this.aforoActual = aforoActual;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

