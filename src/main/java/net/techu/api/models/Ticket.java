package net.techu.api.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.Date;


@Document(collection = "Ticket")
@JsonPropertyOrder({"idOffice", "typeDocument", "numberDocument","isPreferential","numberTicket","creationDatetime"})
public class Ticket implements Serializable {

    //@NotNull
    @JsonProperty("idOffice")
    private String idOffice;

    //@NotNull
    @JsonProperty("typeDocument")
    private String typeDocument;

    //@NotNull
    @JsonProperty("numberDocument")
    private String numberDocument;

    @JsonProperty("isPreferential")
    private String isPreferential;

    @JsonProperty("numberTicket")
    private long numberTicket;

    @JsonProperty("creationDatetime")
    @DateTimeFormat(style="yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private Date fechaHora;

    public Ticket() {
    }

    public Ticket(String idOffice, String typeDocument, String numberDocument, String isPreferential) {
        this.idOffice = idOffice;
        this.typeDocument = typeDocument;
        this.numberDocument = numberDocument;
        this.isPreferential = isPreferential;
    }

    public String getIdOffice() {
        return idOffice;
    }

    public void setIdOffice(String idOffice) {
        this.idOffice = idOffice;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public String getNumberDocument() {
        return numberDocument;
    }

    public void setNumberDocument(String numberDocument) {
        this.numberDocument = numberDocument;
    }

    public String getIsPreferential() {
        return isPreferential;
    }

    public void setIsPreferential(String isPreferential) {
        this.isPreferential = isPreferential;
    }

    public long getNumberTicket() {
        return numberTicket;
    }

    public void setNumberTicket(long numberTicket) {
        this.numberTicket = numberTicket;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }
}
