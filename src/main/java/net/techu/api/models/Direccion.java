package net.techu.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(value = { "state","city","zipCode","country", "geographicGroups"})
public class Direccion implements Serializable {
    @JsonProperty("addressName")
    private String addressName;
    @JsonProperty("state")
    private Estado state;
    @JsonProperty("city")
    private String city;
    @JsonProperty("zipCode")
    private String zipCode;
    @JsonProperty("country")
    private Pais country;
    @JsonProperty("geolocation")
    private Geolocalizacion geolocation;
    @JsonProperty("geographicGroups")
    private List<GeographicGroups> geographicGroups;

    public List<GeographicGroups> getGeographicGroups() {
        return geographicGroups;
    }

    public void setGeographicGroups(List<GeographicGroups> geographicGroups) {
        this.geographicGroups = geographicGroups;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public Estado getState() {
        return state;
    }

    public void setState(Estado state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Pais getCountry() {
        return country;
    }

    public void setCountry(Pais country) {
        this.country = country;
    }

    public Geolocalizacion getGeolocation() {
        return geolocation;
    }

    public void setGeolocation(Geolocalizacion geolocation) {
        this.geolocation = geolocation;
    }


}
