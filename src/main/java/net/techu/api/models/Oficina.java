package net.techu.api.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;

@JsonPropertyOrder({"id","address", "distance","description","isOpen","telephone","capacity"})
@JsonIgnoreProperties(value = { "open","distance","telephone" })
//@JsonIgnoreProperties(ignoreUnknown = true)
public class Oficina implements Serializable {

    @JsonProperty("id")
    private String id;
    @JsonProperty("distance")
    private String distance;
    @JsonProperty("description")
    private String description;
    @JsonProperty("isOpen")
    private boolean isOpen;
    @JsonProperty("telephone")
    private String telephone;
    @JsonProperty("address")
    private Direccion address;
    @JsonProperty("capacity")
    private Capacidad capacidad;

    public Capacidad getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Capacidad capacidad) {
        this.capacidad = capacidad;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Direccion getAddress() {
        return address;
    }

    public void setAddress(Direccion address) {
        this.address = address;
    }
}
