package net.techu.api.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RespuestaListadoOficina implements Serializable {
    @JsonProperty("data")
    private List<Oficina> data;

    public List<Oficina> getData() {
        return data;
    }

    public void setData(List<Oficina> data) {
        this.data = data;
    }
}
